import unittest
import requests
import os
# These are the installation tests for ngnix sever
# and phpmyadmin.
class RunInstallationTests(unittest.TestCase):

    def test_nginx_installation(self):
        
        nginx = "http://127.0.0.1:2354/images/logo.png"
        nginx_status = requests.get(nginx)
        
        self.assertEqual(nginx_status.status_code, 200)
        
    # def test_phpMyAdmin_installation(self):
        
    #     phpMyAdmin = "http://127.0.0.1:2355"
    #     phpMyAdmin_status = requests.get(phpMyAdmin)
        
    #     self.assertEqual(phpMyAdmin_status.status_code, 200)

if __name__ == '__main__':
    unittest.main()
