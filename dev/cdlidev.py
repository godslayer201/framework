#!/usr/bin/env python
"""
CDLI Framework Development CLI

Usage:
  cdlidev.py [-nd] up [--] [<compose_args>...]
  cdlidev.py [-n]  down [--] [<compose_args>...]
  cdlidev.py [-nd] restart [--] [<compose_args>...]
  cdlidev.py [-n]  destroy [--] [<compose_args>...]
  cdlidev.py [-n] -- <compose_args>...
  cdlidev.py (-v | --version)
  cdlidev.py (-h | --help)

Commands:
  up              Create, start, or update the development stack.
  down            Stop and remove containers/networks in the development stack.
  restart         Run 'down' then 'up' (<compose_args> provided to 'up' only).
  destroy         Like 'down' but also deletes data volumes.
  <none>          Pass arbitrary commands/arguments through to Docker Compose.

Options:
  -d --detach     Run the development stack in the background.
  -n --dry-run    Show but don't execute any external commands.
  -v --version    Show program version.
  -h --help       Show this screen.
"""

#
# Imports
#
from __future__ import absolute_import, print_function

import json
import os
import platform
import sys
import re

try:
    from docopt import docopt
    from six.moves import input, shlex_quote
except ImportError as e:
    print("Missing dependency: %s." % e)
    print("Please ensure that Docker Compose, six, and docopt are installed.")
    print("(`pip install docker-compose` should provide all of these.)")
    sys.exit(1)


# Version Constant
__VERSION__ = "v0.2.2 (%s %s, %s)" % (
    platform.python_implementation(),
    platform.python_version(),
    platform.platform()
)


#
# Main function
#
def main():
    os.chdir(os.path.dirname(__file__) or ".")
    args = docopt(__doc__, help=True, version=__VERSION__, options_first=False)
    if(os.path.isfile("./config.json")):
        config_file = "./config.json"
    else:
        print("Using default configuration.")
        config_file = "./config.json.dist"

    try:
        with open(config_file, "r") as f:
            config = json.load(f)
    except Exception as e:
        print("Error loading configuration: %s" % e)
        config = {}

    return run_cmd(args, config)


#
# Compile and run command.
#
def run_cmd(args, config):  # noqa: C901
    createDockerCompose(config)

    cmd = ["docker-compose", "--project-name", "cdlidev"]

    if args["restart"] and not args["up"]:
        tmp_args = args.copy()
        tmp_args.update({
          "restart": False,
          "down": True,
          "<compose_args>": []
        })
        run_cmd(tmp_args, config)
        args["up"] = True

    if args["up"]:
        cmd.append("up")
        if args["--detach"]:
            cmd.append("-d")

        for serviceType in ['infrastructure', 'app', 'extra']:
            tempContainers = []
            for container in config.get(serviceType, {}):
                is_enable = bool(dig(config, (serviceType, container, "enabled"), True))
                is_default = bool(dig(config, (serviceType, container, "is_default"), True))
                if (is_default or is_enable):
                    scale = int(dig(config, (serviceType, container, "scale")))
                    if (serviceType == 'app'):
                        cmd.extend(["--scale", "app_%s=%s" % (container, scale)])
                    else:
                        cmd.extend(["--scale", "%s=%s" % (container, scale)])

    elif args["down"]:
        cmd.append("down")

    elif args["destroy"]:
        question = ("WARNING: This will delete all existing project "
                    "data volumes (including MariaDB).\n"
                    "Are you sure you want to do this? (\"yes\" to confirm): ")
        if str(input(question)).lower().strip() == "yes":
            cmd.extend(["down", "--volumes"])
        else:
            print("Command aborted.")
            return 1

    if args["<compose_args>"]:
        cmd.extend(args["<compose_args>"])

    cmd_line = " ".join(shlex_quote(param) for param in cmd)

    if args["--dry-run"]:
        return print("(dry-run) $ %s" % cmd_line)

    if cmd_line:
        print("$ %s" % cmd_line)
        return os.system(cmd_line)

#
# Create docker-compose.yml dynamically
#
def createDockerCompose(config) :
    containers = {}

    for serviceType in ['infrastructure', 'app', 'extra']:
        tempContainers = []
        for container in config.get(serviceType, {}):
            is_enable = bool(dig(config, (serviceType, container, "enabled"), True))
            is_default = bool(dig(config, (serviceType, container, "is_default"), True))
            if (is_default or is_enable):
                tempContainers.append(container)
        containers[serviceType] = tempContainers

    # Creating docker-compose.yml file
    orgDComposeFile = open("docker-compose-org.yml", "r")
    dcFile = open("docker-compose.yml", "w")
    writeFlag = 0
    pattern = re.compile("^# Image--*")
    for line in orgDComposeFile.readlines():
        if ((re.compile("^version:")).match(line.strip())):
            writeFlag = 1
        elif (pattern.match(line.strip())):
            serviceType, containerName = (line.strip()[9:]).split(":")
            if (containerName in containers[serviceType]):
                writeFlag = 1
            else:
                writeFlag = 0

        if (line == '\n'):
            writeFlag = 1

        if (writeFlag):
            dcFile.write(line)

    dcFile.close()
    orgDComposeFile.close()

#
# Utility functions
#
def dig(obj, keys, default=None):
    """Dig recursively by key in a dict/object."""
    try:
        for key in keys:
            obj = obj[key]
    except (IndexError, KeyError, TypeError):
        obj = default
    return obj

if __name__ == "__main__":
    sys.exit(main())
