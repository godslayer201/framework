<?php
namespace App\View\Helper;

use Cake\View\Helper;

class ToggleHelper extends Helper
{
    public function switch($id, $name, $checked = false)
    {
        if ($checked == true) {
            return implode("\n", [
            '<label class="css-toggle">',
            '<input type="checkbox" checked id="'.h($id).'" name="'.h($name).'">',
            '<span class="toggle-slider round"></span>',
            '</label>'
        ]);
        } else {
            return implode("\n", [
            '<label class="css-toggle">',
            '<input type="checkbox" id="' . h($id) . '" name="' . h($name) . '">',
            '<span class="toggle-slider round"></span>',
            '</label>'
        ]);
        }
    }
}
