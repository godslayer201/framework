<?php
namespace App\View;

use Cake\View\SerializedView;

class RdfJsonView extends SerializedView
{
    use LinkedDataTrait;

    protected $_responseType = 'rdfjson';

    protected function _serialize($serialize)
    {
        $data = $this->_dataToSerialize($serialize);
        $graph = $this->prepareDataExport($data);
        return $graph->serialise('json');
    }
}
