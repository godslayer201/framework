<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ArtifactType Entity
 *
 * @property int $id
 * @property string $artifact_type
 * @property int|null $parent_id
 *
 * @property \App\Model\Entity\ArtifactType $parent_artifact_type
 * @property \App\Model\Entity\ArtifactType[] $child_artifact_types
 * @property \App\Model\Entity\Artifact[] $artifacts
 */
class ArtifactType extends Entity
{
    use LinkedDataTrait;

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'artifact_type' => true,
        'parent_id' => true,
        'parent_artifact_type' => true,
        'child_artifact_types' => true,
        'artifacts' => true
    ];

    public function getCidocCrm()
    {
        return [
            '@id' => $this->getUri(),
            '@type' => 'cdli:type_artifact_type',
            'rdfs:label' => $this->artifact_type,
            'crm:P127_has_broader_term' => $this->getUri($this->parent_id),
            'crm:P127i_has_narrower_term' => self::getUris($this->child_artifact_types),
            'crm:P2i_is_type_of' => self::getEntities($this->artifacts)
        ];
    }
}
