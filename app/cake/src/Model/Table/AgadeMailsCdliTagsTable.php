<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * AgadeMailsCdliTags Model
 *
 * @property \App\Model\Table\AgadeMailsTable|\Cake\ORM\Association\BelongsTo $AgadeMails
 * @property \App\Model\Table\CdliTagsTable|\Cake\ORM\Association\BelongsTo $CdliTags
 *
 * @method \App\Model\Entity\AgadeMailsCdliTag get($primaryKey, $options = [])
 * @method \App\Model\Entity\AgadeMailsCdliTag newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\AgadeMailsCdliTag[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\AgadeMailsCdliTag|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\AgadeMailsCdliTag|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\AgadeMailsCdliTag patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\AgadeMailsCdliTag[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\AgadeMailsCdliTag findOrCreate($search, callable $callback = null, $options = [])
 */
class AgadeMailsCdliTagsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('agade_mails_cdli_tags');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('AgadeMails', [
            'foreignKey' => 'agade_mail_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('CdliTags', [
            'foreignKey' => 'cdli_tag_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['agade_mail_id'], 'AgadeMails'));
        $rules->add($rules->existsIn(['cdli_tag_id'], 'CdliTags'));

        return $rules;
    }
}
