<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>
<div class="row justify-content-md-center">

    <div class="col-lg-7 boxed">
        <div class="capital-heading"><?= __('Edit User') ?></div>

        <?= $this->Flash->render() ?>

        <?= $this->Form->create("user",['type'=>'post']) ?>
            <table class="table-bootstrap">
                <tbody>
                    <tr>
                        <th scope="row"><?= __('Username') ?></th>
                        <td><?= h($user['username']) ?></td>
                    </tr>
                    <tr>
                        <th scope="row"><?= __('Email') ?></th>
                        <td><?= h($user['email']) ?></td>
                    </tr>
                    <tr>
                        <th scope="row"><?= __('Created') ?></th>
                        <td><?= h($user['created_at']) ?></td>
                    </tr>
                    <tr>
                        <th scope="row"><?= __('Admin') ?></th>
                        <td>  
                            <?= in_array(1, $user['roles']) ? $this->Toggle->switch("roles", "1", true) : $this->Toggle->switch("roles", "1") ?>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row"><?= __('Editor') ?></th>
                        <td>  
                            <?= in_array(2, $user['roles']) ? $this->Toggle->switch("roles", "2", true) : $this->Toggle->switch("roles", "2") ?>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row"><?= __('Can Download HD Images') ?></th>
                        <td>  
                            <?= in_array(3, $user['roles']) ? $this->Toggle->switch("roles", "3", true) : $this->Toggle->switch("roles", "2") ?>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row"><?= __('Can View Private Artifacts') ?></th>
                        <td>  
                            <?= in_array(4, $user['roles']) ? $this->Toggle->switch("roles", "4", true) : $this->Toggle->switch("roles", "4") ?>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row"><?= __('Can View Private Inscriptions') ?></th>
                        <td>  
                            <?= in_array(5, $user['roles']) ? $this->Toggle->switch("roles", "5", true) : $this->Toggle->switch("roles", "5") ?>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row"><?= __('Can Edit Transliterations') ?></th>
                        <td>  
                            <?= in_array(6, $user['roles']) ? $this->Toggle->switch("roles", "6", true) : $this->Toggle->switch("roles", "6") ?>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row"><?= __('Can View Private Images') ?></th>
                        <td>  
                            <?= in_array(7, $user['roles']) ? $this->Toggle->switch("roles", "7", true) : $this->Toggle->switch("roles", "7") ?>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row"><?= __('Can View CDLI Tablet Manager') ?></th>
                        <td>  
                            <?= in_array(8, $user['roles']) ? $this->Toggle->switch("roles", "8", true) : $this->Toggle->switch("roles", "8") ?>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row"><?= __('Can View CDLI Forums') ?></th>
                        <td>  
                            <?= in_array(9, $user['roles']) ? $this->Toggle->switch("roles", "9", true) : $this->Toggle->switch("roles", "9") ?>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row"><?= __('Active') ?></th>
                        <td>  
                            <?= $user['active'] ? $this->Toggle->switch("0", "active", true) : $this->Toggle->switch("0", "active") ?>
                        </td>
                    </tr>
                </tbody>
            </table>
            <?= $this->Form->submit() ?>
        <?= $this->Form->end() ?>

    </div>

    <div class="col-lg boxed">
        <div class="capital-heading"><?= __('Related Actions') ?></div>
        <?= $this->Html->link(__('View this profile'), ['action' => 'view', $user['username']], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('View all Users'), ['action' => 'index'], ['class' => 'btn-action']) ?>
    </div>

</div>
