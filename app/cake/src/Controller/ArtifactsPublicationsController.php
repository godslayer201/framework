<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * ArtifactsPublications Controller
 *
 * @property \App\Model\Table\ArtifactsPublicationsTable $ArtifactsPublications
 *
 * @method \App\Model\Entity\ArtifactsPublication[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ArtifactsPublicationsController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('RequestHandler');
        $this->loadComponent('TableExport');
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Artifacts', 'Publications']
        ];
        $artifactsPublications = $this->paginate($this->ArtifactsPublications);

        $this->set(compact('artifactsPublications'));
        $this->set('_serialize', 'artifactsPublications');
    }

    /**
     * View method
     *
     * @param string|null $id Artifacts Publication id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $artifactsPublication = $this->ArtifactsPublications->get($id, [
            'contain' => ['Artifacts', 'Publications']
        ]);

        $this->set('artifactsPublication', $artifactsPublication);
        $this->set('_serialize', 'artifactsPublication');
    }
}
