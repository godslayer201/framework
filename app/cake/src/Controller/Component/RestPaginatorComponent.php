<?php
namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Event\Event;
use Cake\Routing\Router;

class RestPaginatorComponent extends Component
{
    public $components = ['Paginator'];

    public function beforeRender(Event $event)
    {
        $controller = $this->_registry->getController();

        if (!isset($controller->viewVars['_serialize'])) {
            return;
        }

        $request = $controller->getRequest();
        $params = $this->Paginator->_paginator->getPagingParams();

        if (empty($params)) {
            return;
        }

        $params = array_values($params)[0];

        $links = [
            $this->buildUrl('1', $params['perPage'], 'first'),
            $this->buildUrl($params['page'], $params['perPage'], 'current'),
            $this->buildUrl($params['pageCount'], $params['perPage'], 'last')
        ];

        if ($params['prevPage']) {
            $links[] = $this->buildUrl($params['page'] - 1, $params['perPage'], 'prev');
        }

        if ($params['nextPage']) {
            $links[] = $this->buildUrl($params['page'] + 1, $params['perPage'], 'next');
        }

        $controller->response = $controller->response->withHeader('Link', $links);
    }

    private function buildUrl($page, $perPage, $rel)
    {
        $url = Router::url(['?' => array_merge(
            $this->request->getQueryParams(),
            [
                'page' => $page,
                'limit' => $perPage
            ]
        )], true);

        return '<' . $url . '>; rel="' . $rel . '"';
    }
}
