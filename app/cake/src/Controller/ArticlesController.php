<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Datasource\ConnectionManager;
use Cake\Http\Exception\ForbiddenException;

/**
 * Articles Controller
 *
 * @property \App\Model\Table\JournalsTable $Journals
 *
 * @method \App\Model\Entity\Journal[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ArticlesController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadModel('Articles');
        $this->loadComponent('RequestHandler');
        $this->loadComponent('Bibliography');

        $this->Auth->allow(['index', 'view', 'image']);
    }

    public function index($type)
    {
        $this->paginate = [
            'contain' => ['Authors']
        ];
        $articles = $this->paginate($this->Articles->find('type', ['type' => $type]));

        $this->set(compact('articles', 'type'));
        $this->set('_serialize', 'articles');
    }

    public function view($id)
    {
        $article = $this->Articles->get($id);
        $prefers = $this->RequestHandler->prefers();

        if ($prefers == 'pdf') {
            return $this->response
                ->withFile('webroot/pubs/' . $article->article_type . '/' . $article->pdf_link);
        } elseif ($prefers == 'html') {
            return $this->response
                ->withStringBody($article->content_html)
                ->withType('html');
        }

        $this->set('article', $article);
        $this->set('_serialize', 'article');
    }

    public function image($id, $name)
    {
        return $this->response->withFile('webroot/pubs/' . $id . '/images/' . $name);
    }
}
