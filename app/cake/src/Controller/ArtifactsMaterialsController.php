<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * ArtifactsMaterials Controller
 *
 * @property \App\Model\Table\ArtifactsMaterialsTable $ArtifactsMaterials
 *
 * @method \App\Model\Entity\ArtifactsMaterial[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ArtifactsMaterialsController extends AppController
{
    /**
     * intialize method
     *
     * @return \Cake\Http\Response|void
     */
    public function initialize()
    {
        parent::initialize();

        // Set access for public.
        $this->Auth->allow(['index', 'view']);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Artifacts', 'Materials', 'MaterialColors', 'MaterialAspects']
        ];
        $artifactsMaterials = $this->paginate($this->ArtifactsMaterials);

        $this->set(compact('artifactsMaterials'));
        $this->set('_serialize', 'artifactsMaterials');
    }

    /**
     * View method
     *
     * @param string|null $id Artifacts Material id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $artifactsMaterial = $this->ArtifactsMaterials->get($id, [
            'contain' => ['Artifacts', 'Materials', 'MaterialColors', 'MaterialAspects']
        ]);

        $this->set('artifactsMaterial', $artifactsMaterial);
        $this->set('_serialize', 'artifactsMaterial');
    }
}
