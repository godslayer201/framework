<?php
namespace App\Controller;

use App\Controller\AppController;
use GoogleAuthenticator\GoogleAuthenticator;
use Cake\I18n\Time;

/**
 * TwofactorController Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class TwofactorController extends AppController
{
    /**
     * intialize method
     *
     * @return \Cake\Http\Response|void
     */
    public function initialize()
    {
        parent::initialize();

        // Load Component 'GeneralFunctions'
        $this->loadComponent('GeneralFunctions');
    }

    /**
     * beforeFilter method
     *
     * To set up access before this contoller is executed.
     *
     * @return \Cake\Http\Response|void
     */
    public function beforeFilter(\Cake\Event\Event $event)
    {
        parent::beforeFilter($event);

        // Set access for public view.
        $this->Auth->allow(['index']);
    }

    /**
     * Index method (2FA Setup Method)
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        // Get Session
        $session = $this->getRequest()->getSession();

        // Check if session 'user' is present then execute next logic. If not then redirect to '/'
        if ($session->read('user.user') != null) {
            $session_user = $session->read('user');

            // If present session is verified then redirect to '/'
            if ($session->read('session_verified') != null) {
                return $this->redirect([
                    "controller" => "Home",
                    "action" => "index"
                ]);
            } else {
                // Check if 2FA Enabled or not. If enabled redirect to verify page or redirect to 2FA setup page.
                $redirectToVerify = $session_user['type'] === 'login' && $session_user['user']['2fa_status'];

                if ($redirectToVerify) {
                    $this->setAction("twofaverify");
                } else {
                    $this->loadModel('Users');
                    $this->loadComponent('GoogleAuthenticator');
                    $ga = $this->GoogleAuthenticator;

                    if ($this->request->is('post')) {
                        $checkconfirm = $this->request->data['checkconfirm'];

                        if ($checkconfirm == 0) {
                            $this->Flash->error(__('Please back up your 16-digit key before proceeding.'), ['class' => 'alert alert-danger']);

                            return $this->redirect([
                                "controller" => "Twofactor",
                                "action" => "index"
                            ]);
                        }

                        $secret = $this->request->data['secretcode'];
                        $oneCode = $this->request->data['code'];
                        $checkResult = $ga->verifyCode($secret, $oneCode, 2);    // 2 = 2*30sec clock tolerance

                        // For Development mode
                        $ifDevelopment = (bool)(env('APP_ENV') === 'development');

                        if ($checkResult || $ifDevelopment) {
                            $time_now = Time::now();

                            // If requested from Login
                            if ($session_user['type'] === 'login') {
                                $updateUser['2fa_key'] =  $this->request->data['secretcode'];
                                $updateUser['2fa_status'] = 1;
                                $updateUser['last_login_at'] = $time_now;
                                $updateUser['modified_at'] = $time_now;

                                $retrivedUser = $this->Users->find('all', [
                                    'conditions' => [
                                        'Users.username' => $session_user['user']['username']
                                    ]
                                ])->first();

                                $saveUser = $this->Users->patchEntity($retrivedUser, $updateUser);
                            } else {
                                $modifiedData['2fa_key'] =  $this->request->data['secretcode'];
                                $modifiedData['2fa_status'] = 1;
                                $modifiedData['last_login_at'] = $time_now;
                                $modifiedData['modified_at'] = $time_now;
                                $modifiedData['active'] = 1;
                                $modifiedData['created_at'] = $session_user['user']['created_at'];

                                $saveUser = $this->Users->patchEntity($session_user['user'], $modifiedData);

                                if (!empty($saveUser->errors())) {
                                    foreach ($saveUser->errors() as $error) {
                                        foreach ($error as $key => $value) {
                                            $this->Flash->error($value);
                                        }
                                    }

                                    return $this->redirect([
                                        'controller' => 'Users',
                                        'action' => 'register'
                                    ]);
                                }
                            }

                            if ($this->Users->save($saveUser)) {
                                if ($session->read('user.email') == null) {
                                    // When User Logs in
                                    $this->AuthenticateUsersRole($session_user['user']['username']);
                                } else {
                                    // When User Registers
                                    $this->AuthenticateUsersRole($session_user['user']['username'], 1);
                                }

                                $session->delete('user');

                                $this->Flash->success(__('Two-Factor Authentication (2FA) Is Enabled.'), ['class' => 'alert alert-danger']);

                                return $this->redirect([
                                    "controller" => "Home",
                                    "action" => "index"
                                ]);
                            } else {
                                return $this->Flash->error(__('Please try again.'), ['class' => 'alert alert-danger']);
                            }
                        } else {
                            return $this->Flash->error(__('Wrong code entered.Please try again.'), ['class' => 'alert alert-danger']);
                        }
                    }
                }
            }
        } else {
            return $this->redirect([
                "controller" => "Home",
                "action" => "index"
            ]);
        }
    }

    /**
     * 2FA Verify method
     *
     * @return \Cake\Http\Response|void
     */
    public function twofaverify()
    {
        $this->loadComponent('GoogleAuthenticator');
        $this->loadModel('Users');
        $ga = $this->GoogleAuthenticator;
        $user = $this->getRequest()->getSession()->read('user.user');

        // Remove the following line and $ifDevelopment variable while checking result (Line no. 188) in production.
        $ifDevelopment = (bool)(env('APP_ENV') === 'development');

        if ($this->request->is('post')) {
            $secret = $this->get2faKey($user['username']);
            $oneCode = $this->request->data['code'];
            $checkResult = $ga->verifyCode($secret, $oneCode, 2);

            if ($checkResult || $ifDevelopment) {
                $updateUser['last_login_at'] = Time::now();

                $retrivedUser = $this->getUser($user['username']);

                $saveUser = $this->Users->patchEntity($retrivedUser, $updateUser);

                if (empty($saveUser->errors()) && $this->Users->save($saveUser)) {
                    $this->AuthenticateUsersRole($user['username']);
                    $this->getRequest()->getSession()->delete('user');

                    return $this->redirect([
                        'controller' => 'Home',
                        'action' => 'index'
                    ]);
                } else {
                    $this->getRequest()->getSession()->delete('user');
                    $this->Flash->error(__("Error on Server Side !!"));
                    return $this->redirect(['controller' => 'Home', 'action' => 'index']);
                }
            } else {
                $this->Flash->error(__('Wrong code entered.Please try again.'), [
                    'class' => 'alert alert-danger'
                ]);
            }
        }
    }

    // Retrive user from DB
    public function getUser($username)
    {
        $this->loadModel('Users');
        $user = $this->Users->find(
            'all',
            [
            'conditions' => [
                'Users.username' => $username]
            ]
        )->first();
        return $user;
    }

    // Authenticates User's Login or Registers.
    public function AuthenticateUser($user, $roles)
    {
        $this->getRequest()->getSession()->write('session_verified', 1);

        $authUser['id'] = $user['id'];
        $authUser['username'] = $user['username'];
        $authUser['email'] = $user['email'];
        $authUser['author_id'] = $user['author_id'];
        $authUser['last_login_at'] = $user['last_login_at'];
        $authUser['active'] = $user['active'];
        $authUser['modified_at'] = $user['modified_at'];
        $authUser['hd_images_collection_id'] = $user['hd_images_collection_id'];
        $authUser['roles'] = $roles;

        $this->Auth->setUser($authUser);
    }

    // Returns 2FA key for specific username
    public function get2faKey($username)
    {
        $user = $this->getUser($username);
        return $user['2fa_key'];
    }

    /**
     * Set/Get User's Role method on successful GET, Authenticate User
     *
     * SET Role (Registration) and GET Role (Registration , Login)
     *
     * @param : $username, $type
     * username -> Username
     * type -> 1 for SET
     *
     * @return : void
     */
    public function AuthenticateUsersRole($username, $type = null)
    {
        $this->loadModel('RolesUsers');

        $user = $this->getUser($username);

        // SET User Role
        if ($type == 1) {
            // By default setting it to empty array
            $roles = [];
        } else {
            // GET User Role
            $roles = $this->GeneralFunctions->getUsersRole($user['id']);
        }
        $this->AuthenticateUser($user, $roles);
    }

    /**
     * Set Roles
     *
     * @param : $userId, $toBeSet, $toBeRemoved
     * userId -> User ID
     * toBeSet -> Roles to be added (array)
     * toBeRemoved -> Roles to be removed (array)
     *
     * @return : array of roles
     */
    public function setUsersRole($userId, $toBeSet = null, $toBeRemoved = null)
    {
        $this->loadModel('RolesUsers');

        if (!is_null($toBeRemoved)) {
            if (!$this->RolesUsers->deleteAll(['user_id' => $userId, 'role_id IN' => $toBeRemoved])) {
                $this->Flash->error(__("Error in setting Roles !!"));
            };
        }

        if (!is_null($toBeSet)) {
            $userRole = [];

            foreach ($toBeSet as $role_id) {
                array_push($userRole, ['user_id' => $userId, 'role_id' => $role_id]);
            }

            if (!$this->RolesUsers->saveMany($this->RolesUsers->newEntities($userRole))) {
                $this->Flash->error(__("Error in setting Roles !!"));
            }
        }

        return $this->GeneralFunctions->getUsersRole($userId);
    }
}
