<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * ExternalResources Controller
 *
 * @property \App\Model\Table\ExternalResourcesTable $ExternalResources
 *
 * @method \App\Model\Entity\ExternalResource[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ExternalResourcesController extends AppController
{
    /**
     * intialize method
     *
     * @return \Cake\Http\Response|void
     */
    public function initialize()
    {
        parent::initialize();

        // Load Component 'GeneralFunctions'
        $this->loadComponent('GeneralFunctions');
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        // Access Check
        if (!$this->GeneralFunctions->checkIfRolesExists([1])) {
            $this->Flash->error($this->Auth->config('authError'));
            return $this->redirect($this->referer());
        }

        $externalResources = $this->paginate($this->ExternalResources);

        $this->set(compact('externalResources'));
    }

    /**
     * View method
     *
     * @param string|null $id External Resource id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        // Access Check
        if (!$this->GeneralFunctions->checkIfRolesExists([1])) {
            $this->Flash->error($this->Auth->config('authError'));
            return $this->redirect($this->referer());
        }

        $externalResource = $this->ExternalResources->get($id, [
            'contain' => []
        ]);

        $artifacts = $this->loadModel('artifacts_external_resources');
        $count = $artifacts->find('list', ['conditions' => ['external_resource_id' => $id]])->count();

        $this->set(compact('externalResource', 'count'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        // Access Check
        if (!$this->GeneralFunctions->checkIfRolesExists([1])) {
            $this->Flash->error($this->Auth->config('authError'));
            return $this->redirect($this->referer());
        }

        $externalResource = $this->ExternalResources->newEntity();
        if ($this->request->is('post')) {
            $externalResource = $this->ExternalResources->patchEntity($externalResource, $this->request->getData());
            if ($this->ExternalResources->save($externalResource)) {
                $this->Flash->success(__('The external resource has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The external resource could not be saved. Please, try again.'));
        }
        $artifacts = $this->ExternalResources->Artifacts->find('list', ['limit' => 200]);
        $this->set(compact('externalResource', 'artifacts'));
    }

    /**
     * Edit method
     *
     * @param string|null $id External Resource id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        // Access Check
        if (!$this->GeneralFunctions->checkIfRolesExists([1])) {
            $this->Flash->error($this->Auth->config('authError'));
            return $this->redirect($this->referer());
        }

        $externalResource = $this->ExternalResources->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $externalResource = $this->ExternalResources->patchEntity($externalResource, $this->request->getData());
            if ($this->ExternalResources->save($externalResource)) {
                $this->Flash->success(__('The external resource has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The external resource could not be saved. Please, try again.'));
        }
        $artifacts = $this->ExternalResources->Artifacts->find('list', ['limit' => 200]);
        $this->set(compact('externalResource', 'artifacts'));
    }

    /**
     * Delete method
     *
     * @param string|null $id External Resource id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        // Access Check
        if (!$this->GeneralFunctions->checkIfRolesExists([1])) {
            $this->Flash->error($this->Auth->config('authError'));
            return $this->redirect($this->referer());
        }

        $this->request->allowMethod(['post', 'delete']);
        $externalResource = $this->ExternalResources->get($id);
        if ($this->ExternalResources->delete($externalResource)) {
            $this->Flash->success(__('The external resource has been deleted.'));
        } else {
            $this->Flash->error(__('The external resource could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
