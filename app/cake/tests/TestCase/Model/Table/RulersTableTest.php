<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\RulersTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\RulersTable Test Case
 */
class RulersTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\RulersTable
     */
    public $Rulers;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.rulers',
        'app.periods',
        'app.dynasties',
        'app.dates'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Rulers') ? [] : ['className' => RulersTable::class];
        $this->Rulers = TableRegistry::getTableLocator()->get('Rulers', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Rulers);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
