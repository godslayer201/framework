<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\AccountingPeriodsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\AccountingPeriodsTable Test Case
 */
class AccountingPeriodsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\AccountingPeriodsTable
     */
    public $AccountingPeriods;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.accounting_periods',
        'app.rulers',
        'app.months',
        'app.years'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('AccountingPeriods') ? [] : ['className' => AccountingPeriodsTable::class];
        $this->AccountingPeriods = TableRegistry::getTableLocator()->get('AccountingPeriods', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->AccountingPeriods);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
