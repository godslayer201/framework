import cv2
import json
import sys
import os


class Archival:

    def __init__(self, img, rotations, pno, gutter_ratio=2):
        self.empty_img = [[0, 0, 0], [0, 0, 0], [0, 0, 0]]
        self.pno = pno
        self.imgs = {
            'img_le': cv2.imread(img['LE']) if img['LE'] else self.empty_img,
            'img_be': cv2.imread(img['BE']) if img['BE'] else self.empty_img,
            'img_r': cv2.imread(img['R']) if img['R'] else self.empty_img,
            'img_o': cv2.imread(img['O']) if img['O'] else self.empty_img,
            'img_re': cv2.imread(img['RE']) if img['RE'] else self.empty_img,
            'img_te': cv2.imread(img['TE']) if img['TE'] else self.empty_img,
        }
        self.rotations = rotations
        self.gutter_ratio = gutter_ratio
        self.relative_x = None
        self.relative_y = None
        self.img_archival = [[0, 0, 0], [0, 0, 0], [0, 0, 0]]

    def vconcat_resize_min(self, im_list, interpolation=cv2.INTER_CUBIC):
        w_min = self.relative_x
        im_list_resize = [cv2.resize(im, (w_min, int(im.shape[0] * w_min / im.shape[1])), interpolation=interpolation)
                          for im in im_list]
        self.vResizeList = im_list_resize
        return cv2.vconcat(im_list_resize)

    def hconcat_resize_min(self, im_list, interpolation=cv2.INTER_CUBIC):
        h_min = self.relative_y
        im_list_resize = [cv2.resize(im, (int(im.shape[1] * h_min / im.shape[0]), h_min), interpolation=interpolation)
                          for im in im_list]
        self.hResizeList = im_list_resize
        return cv2.hconcat(im_list_resize)

    # def hconcat_resize_abs(self, im_list, main_im, size, interpolation=cv2.INTER_CUBIC):
    #     h_min = relative_y
    #     im_list_resize = [cv2.resize(im, (int(im.shape[1] * h_min / im.shape[0]), h_min), interpolation=interpolation)
    #                       for im in im_list]
    #     im_list_padding = [cv2.copyMakeBorder(im, top=0, left=0, right=0, bottom=(main_im.shape[0]-h_min), borderType=cv2.BORDER_CONSTANT,
    #                                           value=[0, 0, 0]) for im in im_list_resize]
    #     return im_list_padding

    def resize_abs(self, img, size, isX, interpolation=cv2.INTER_CUBIC):
        if isX:
            return cv2.resize(img, (size, int(img.shape[0] * size / img.shape[1])), interpolation=interpolation)
        else:
            return cv2.resize(img, (int(img.shape[1] * size / img.shape[0]), size), interpolation=interpolation)

    def stretch(self, img, totalSize, maintainSize, isX):
        if isX:
            return cv2.copyMakeBorder(img, top=0, left=maintainSize-(img.shape[1]//2), right=totalSize-maintainSize-(img.shape[1]//2), bottom=0, borderType=cv2.BORDER_CONSTANT, value=[0, 0, 0])
        else:
            return cv2.copyMakeBorder(img, top=maintainSize-(img.shape[0]//2), left=0, right=0, bottom=totalSize-maintainSize-(img.shape[0]//2), borderType=cv2.BORDER_CONSTANT, value=[0, 0, 0])

    def setRatio(self, relative_x=None, relative_y=None, gutter_x=None, gutter_y=None):
        if relative_x or relative_y or gutter_x or gutter_y:
            # manage ratio and accomodate change
            return True
        # elif len(self.imgs) != 6 :
        #     return False
        else:
            self.relative_y = self.imgs['img_o'].shape[0]
            self.relative_x = self.imgs['img_o'].shape[1]

    def rotateImg(self, img, val):
        val = int(val)
        if(val == 0):
            return img
        valAbs = val % 180
        rotation = None
        if(valAbs == 0):
            rotation = cv2.ROTATE_180
        elif valAbs > 0:
            rotation = cv2.ROTATE_90_CLOCKWISE
        else:
            rotation = cv2.ROTATE_90_COUNTERCLOCKWISE
        return cv2.rotate(img, rotation)

    def run(self):
        self.setRatio()
        # vconcatObj = [
        #     self.rotateImg(self.imgs['img_o'], self.rotations['O']),
        #     self.rotateImg(self.imgs['img_te'], self.rotations['TE']),
        #     self.rotateImg(self.imgs['img_r'], self.rotations['R']),
        #     self.rotateImg(self.imgs['img_be'], self.rotations['BE'])
        # ]
        # main, update_y = self.vconcat_resize_min(vconcatObj)

        # super_main = self.hconcat_resize_abs(
        #     [self.rotateImg(self.imgs['img_le'], self.rotations['LE']),
        #      self.rotateImg(self.imgs['img_re'], self.rotations['RE'])],
        #     main,
        #     update_y
        # )
        # final_main = self.hconcat_resize_min(
        #     [super_main[0], main, super_main[1]])
        # vconcatObj = [
        #     self.rotateImg(self.imgs['img_te'], self.rotations['TE']),
        #     self.rotateImg(self.imgs['img_o'], self.rotations['O']),
        #     self.rotateImg(self.imgs['img_be'], self.rotations['BE']),
        #     self.rotateImg(self.imgs['img_r'], self.rotations['R'])
        # ]
        # hConcatObj = {
        #     "LE" : self.rotateImg(self.imgs['img_le'], self.rotations['LE']),
        #     "RE" : self.rotateImg(self.imgs['img_re'], self.rotations['RE'])
        # }
        # vAxis = self.vconcat_resize_min(vconcatObj)

        # update_y = self.vResizeList[1].shape[0]
        # # update_y = self.imgs['img_o'].shape[0]
        # self.relative_y = update_y
        # self.relative_x = self.vResizeList[1].shape[1]

        # le_resized = self.resize_abs(hConcatObj['LE'], update_y, False)
        # re_resized = self.resize_abs(hConcatObj['RE'], update_y, False)

        # le_padded = self.stretch(le_resized, vAxis, self.vResizeList[0].shape[0], False)
        # re_padded = self.stretch(re_resized, vAxis, self.vResizeList[0].shape[0], False)

        # cv2.imwrite(f'NRle.jpeg', le_resized)
        # cv2.imwrite(f'NRre.jpeg', re_resized)

        # cv2.imwrite(f'NPle.jpeg', le_padded)
        # cv2.imwrite(f'NPre.jpeg', re_padded)

        # cv2.imwrite(f'NVAxis.jpeg', vAxis)

        # finalImg = self.hconcat_resize_min([le_padded, vAxis, re_padded])

        # cv2.imwrite('complete.jpeg', finalImg)

        # filename = f'{self.pno}.jpg'
        # cv2.imwrite(f'/tmp/{filename}', finalImg)
        # return filename

        layer_1 = {
            'LE': self.rotateImg(self.imgs['img_le'], self.rotations['LE']),
            'O': self.rotateImg(self.imgs['img_o'], self.rotations['O']),
            'RE': self.rotateImg(self.imgs['img_re'], self.rotations['RE'])
        }
        O_side_size = layer_1['O'].shape[1]
        layer_1_img = self.hconcat_resize_min(list(layer_1.values()))

        layer_0 = { 'TE' : self.rotateImg(self.imgs['img_te'], self.rotations['TE'])}
        layer_0_img = self.resize_abs(layer_0['TE'], O_side_size, True)
        layer_0_img = self.stretch(layer_0_img, layer_1_img.shape[1], layer_1_img.shape[1]//2, True )

        layer_2 = { 'BE' : self.rotateImg(self.imgs['img_be'], self.rotations['BE'])}
        layer_2_img = self.resize_abs(layer_2['BE'], O_side_size, True)
        layer_2_img = self.stretch(layer_2_img, layer_1_img.shape[1], layer_1_img.shape[1]//2, True )
        
        layer_3 = { 'R' : self.rotateImg(self.imgs['img_r'], self.rotations['R'])}
        layer_3_img = self.resize_abs(layer_3['R'], O_side_size, True)
        layer_3_img = self.stretch(layer_3_img, layer_1_img.shape[1], layer_1_img.shape[1]//2, True )

        img = self.vconcat_resize_min([layer_0_img, layer_1_img, layer_2_img, layer_3_img])

        filename = f'{self.pno}.jpg'
        cv2.imwrite(f'/tmp/{filename}', img)
        return filename


class Job:
    def __init__(self, x, y):
        self.rawJobData = x
        self.minioObjectsCompleted = y[0]
        self.minioObjectsFailed = y[1]
        self.empty_img = [[0, 0, 0], [0, 0, 0], [0, 0, 0]]

    def perform(self):
        print(self.rawJobData)
        self.data = json.loads(self.rawJobData[0])
        inputmap = {
            "BE": self.empty_img,
            "LE": self.empty_img,
            "RE": self.empty_img,
            "TE": self.empty_img,
            "O": self.empty_img,
            "R": self.empty_img
        }
        for i in self.minioObjectsCompleted:
            inputmap[str(i[-21:-19]).replace('_', '')] = i
        rotations = self.data[0]

        n2 = Archival(inputmap, rotations, self.data[1])
        image = n2.run()

        return ([f'Completed arhival for {self.data[1]}'], [('archivals', f'/tmp/{image}', image)])


# imputmap = {
#     'BE': '/tmp/5f6a577d3b9ba_055_BE.jpg.processed.jpeg',
#     'LE': '/tmp/5f6a577e5e23d_055_LE.jpg.processed.jpeg',
#     'RE': '/tmp/5f6a577ea4f0a_055_RE.jpg.processed.jpeg',
#     'TE': '/tmp/5f6a577fbe37d_055_TE.jpg.processed.jpeg',
#     'O': '/tmp/5f6a577f02219_055_O.jpg.processed.jpeg',
#     'R': '/tmp/5f6a577f3b30f_055_R.jpg.processed.jpeg'
# }
# rotations = {'LE': '0', 'O': '270', 'R': '90', 'BE': '0', 'RE': '0', 'TE': '0'}

# n1 = Archival(imputmap, rotations, 25)
# n1.run()
