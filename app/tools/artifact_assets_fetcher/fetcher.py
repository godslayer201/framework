import argparse
import json
import logging
import os
import re
import sys
from collections import OrderedDict

logging.basicConfig(level=logging.DEBUG)

ASSET_DIRECTORIES = ['eps', 'lineart', 'long_translit', 'pdf', 'photo', 'ptm', 'svg', 'tn_lineart', 'tn_photo']


class ArtifactAssetsFetcher:
    def __init__(self, base_url, base_directory, exempted_artifact_ids):
        self.artifacts = {}
        self.base_url = base_url
        self.base_directory = base_directory
        self.exempted_artifact_ids = exempted_artifact_ids

    def get_files(self, directory):
        files = []
        for item in os.listdir(directory):
            files.append(item)

        return files

    def get_asset_directories(self):
        """
        Iterates through ASSET_DIRECTORIES, extracting the asset directories and files
        Returns:
            directories
                type: dict
                example:    {"eps": ["xxxxx.jpg", "yyyyy.jpg"],
                            "lineart": ["xxxxx.jpg", "yyyyy.jpg"],
                            ...
                            }
        """
        directories = {}
        for asset_directory in ASSET_DIRECTORIES:
            directory_path = os.path.join(self.base_directory, asset_directory)
            if os.path.exists(directory_path):
                files = self.get_files(directory_path)
                directories[asset_directory] = files
            else:
                logging.warning("{} directory not found".format(asset_directory))

        return directories

    def extract_artifact_id(self, file):
        """
        Extracts artefact_id from file name
        Args:
            file:

        Returns:
            artefact_id or None
        """
        regex = re.compile(r'P(\d{6}).*')
        pattern_match = regex.match(file)
        if pattern_match:
            artefact_id = pattern_match.group(1)
            return artefact_id

    def add_assets(self):
        """
        Iterates through directories and adds assets to artifact collection
        Returns:
            None
        """
        directories = self.get_asset_directories()
        for directory, files in directories.items():
            logging.info("Working on {} directory".format(directory))
            for file in files:
                artefact_id = self.extract_artifact_id(file)
                self.add_asset_to_artifacts(directory, file, artefact_id)

    def combine_paths(self, directory, file):
        combined_file_path = os.path.join(self.base_url, 'dl', directory, file)
        return combined_file_path

    def add_asset_to_artifacts(self, directory, file, artifact_id):
        """
        Creates asset map, adds asset to it and adds it to the artifacts.
        Appends asset to the asset map if it already exists in artifacts.
        Args:
            directory:
            file:
            artifact_id:
        """
        if artifact_id not in self.exempted_artifact_ids and artifact_id is not None:
            file_path = self.combine_paths(directory, file)
            if self.artifacts.get(artifact_id, None):
                self.artifacts[artifact_id].setdefault(directory, []).append(file_path)
            else:
                assets_map = OrderedDict()
                assets_map['artifact_id'] = artifact_id
                assets_map[directory] = [file_path]
                self.artifacts[artifact_id] = assets_map

    def save_artifacts(self, file_name):
        """
        Saves artifacts to a json file
        Args:
            file_name:

        Returns:
            None
        """
        artifact_list = []
        for assets in self.artifacts.values():
            artifact_list.append(assets)

        json.dump(artifact_list, open(file_name, 'w'), indent=4)


def controller(directory, base_url, file_name, exempted_ids):
    artifact_assets_fetcher = ArtifactAssetsFetcher(base_url, directory, exempted_ids)
    artifact_assets_fetcher.add_assets()
    artifact_assets_fetcher.save_artifacts(file_name)
    logging.info('Completed')


def verify_exempted_id_list(file_path):
    if not os.path.exists(file_path):
        logging.error('\t{} must be a valid path'.format(file_path))
        sys.exit()

    if not os.path.isfile(file_path):
        logging.error('\t{} must be a file'.format(file_path))
        sys.exit()


def read_exempted_id_list(file_path):
    with open(file_path, 'r') as file:
        lines = file.readlines()
        exempted_ids = [line.strip() for line in lines]

    return exempted_ids


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-d', '--directory', help='Path to the dl directory')
    parser.add_argument('-n', '--file_name', default='artifacts.json', help='File name for resulting json file')
    parser.add_argument('-bu', '--base_url', default='cdli.ucla.edu', help='URL to be appended to asset files')
    parser.add_argument('-x', '--exempted_ids', default=[], help='File with the list of exempted artifact ids')
    args = parser.parse_args()
    if not args.directory:
        logging.error('\tPath to the directory must be passed in')
        sys.exit()

    if not os.path.exists(args.directory):
        logging.error('{} must be a valid path'.format(args.directory))
        sys.exit()

    if args.exempted_ids:
        verify_exempted_id_list(args.exempted_ids)
        args.exempted_ids = read_exempted_id_list(args.exempted_ids)

    controller(args.directory, args.base_url, args.file_name, args.exempted_ids)





